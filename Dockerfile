FROM alpine

MAINTAINER Didier FABERT <didier.fabert@gmail.com>

RUN apk add --no-cache --update mariadb mariadb-client && \
    rm -f /var/cache/apk/*

RUN rm -rf /var/lib/mysql/*

COPY entrypoint.sh /entrypoint.sh

VOLUME [ "/var/lib/mysql" ]

EXPOSE 3306

ENTRYPOINT [ "/entrypoint.sh" ]
