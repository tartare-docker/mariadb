#!/bin/sh

DB_NAME=${DB_NAME:-}
DB_USER=${DB_USER:-}
DB_PASS=${DB_PASS:-}

if [ ! -d "/var/lib/mysql/mysql" ]
then
	echo "Installing database..."
	mysql_install_db --user=mysql >/dev/null 2>&1
fi

echo "Starting MySQL server..."
/usr/bin/mysqld_safe --datadir=/var/lib/mysql --socket=/run/mysqld/mysqld.sock >/dev/null 2>&1 &
pid=$!

timeout=30
echo -n "Waiting for database server to accept connections"
while ! /usr/bin/mysqladmin -u root status >/dev/null 2>&1
do
	timeout=$(($timeout - 1))
	if [ $timeout -eq 0 ]; then
	echo -e "\nCould not connect to database server. Aborting..."
	exit 1
	fi
	echo -n "."
	sleep 1
done
echo

if [ ! -z ${DB_USER} ] && [ ! -z ${DB_PASS} ] && [ ! -z ${DB_NAME} ]
then
    mysql -e "CREATE USER '${DB_USER}'@'localhost' IDENTIFIED BY '${DB_PASS}'"
    mysql -e "CREATE USER '${DB_USER}'@'172.%' IDENTIFIED BY '${DB_PASS}'"
    mysql -e "CREATE DATABASE IF NOT EXISTS ${DB_NAME} DEFAULT CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci'"
    mysql -e "GRANT ALL PRIVILEGES ON ${DB_NAME}.* TO '${DB_USER}'@'localhost' IDENTIFIED BY '${DB_PASS}'"
    mysql -e "GRANT ALL PRIVILEGES ON ${DB_NAME}.* TO '${DB_USER}'@'172.%' IDENTIFIED BY '${DB_PASS}'"
else
    echo "WARNING: Running with user root without password !!!"
    mysql -e "CREATE USER 'root'@'172.%'"
    mysql -e "GRANT ALL PRIVILEGES ON *.* TO 'root'@'172.%' WITH GRANT OPTION"
fi
mysql -e "FLUSH PRIVILEGES"
mysql -e "DROP DATABASE IF EXISTS test"

echo "Wait for PID: \"$pid\""
wait $pid
